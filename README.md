# altlib.org Docker

## Description
This is a docker-compose file for standing up the Bellingham Alternative
Library WordPress website. This is mostly for our internal use but can be used
in tandem with our altlibrarian and alttheme projects as well as a matching
database dump to stand up a very similar website.

Docker compose will look for the following files in docker-files:

- 00-altlib_wp.sql - A SQL dump of the site at the time of container instantiation
- wp-content - Any additional wp-content needed
