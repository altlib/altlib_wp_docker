SET @var_user_login = 'segaloco@gmail.com';

SET @var_user_id = (
	SELECT  ID
	FROM wp_users
	WHERE
		user_login = @var_user_login
);

CREATE TABLE record_status (
	id              INT         NOT NULL    AUTO_INCREMENT
	,description    TINYTEXT    NOT NULL
	,created_at     INT         NOT NULL
	,created_by     INT         NOT NULL
	,modified_at    INT         NOT NULL
	,modified_by    INT         NOT NULL
	,PRIMARY KEY (id)
);

INSERT INTO record_status
    (description,   created_at,         created_by, modified_at,        modified_by)
VALUES
    ('Active',      UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id)
    ,('Inactive',   UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id);
	
SET @var_active_record = (
	SELECT id
	FROM record_status
	WHERE
		description = 'Active'
);

CREATE TABLE transaction_method (
	id              INT         NOT NULL    AUTO_INCREMENT
	,description    TINYTEXT    NOT NULL
	,created_at     INT         NOT NULL
	,created_by     INT         NOT NULL
	,modified_at    INT         NOT NULL
	,modified_by    INT         NOT NULL
	,record_status  TINYINT     NOT NULL    DEFAULT '1'
	,PRIMARY KEY (id)
);

INSERT INTO transaction_method
    (description,       created_at,         created_by, modified_at,        modified_by,    record_status)
VALUES
    ('Cash',            UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Check',          UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Credit Card',    UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Paypal',         UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Venmo',          UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Other',          UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record);

CREATE TABLE transaction_type (
	id                  INT         NOT NULL    AUTO_INCREMENT
	,description        TINYTEXT    NOT NULL
	,is_payout          BOOLEAN     NOT NULL
	,patron_required    BOOLEAN     NOT NULL    DEFAULT '0'
	,reason_required    BOOLEAN     NOT NULL    DEFAULT '1'
	,minimum_amount     DOUBLE      NOT NULL    DEFAULT '0'
	,created_at         INT         NOT NULL
	,created_by         INT         NOT NULL
	,modified_at        INT         NOT NULL
	,modified_by        INT         NOT NULL
	,record_status      TINYINT     NOT NULL    DEFAULT '1'
	,PRIMARY KEY (id)
);

INSERT INTO transaction_type
    (description,   	is_payout,  patron_required,    reason_required,    minimum_amount, created_at,         created_by, 	modified_at,        modified_by,		record_status)
VALUES
    ('Membership',  	0,          1,                  0,                  5,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Event',      	0,          0,                  1,                  0,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Store',      	0,          0,                  1,                  0,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Class',      	0,          0,                  1,                  0,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Credit',     	0,          0,                  1,                  0,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('Donation',   	0,          0,                  1,                  0,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
    ,('CSA',        	0,          1,                  0,                  5,              UNIX_TIMESTAMP(),   @var_user_id,   UNIX_TIMESTAMP(),   @var_user_id,       @var_active_record)
	,('Refund',			1,			0,					1,					0,				UNIX_TIMESTAMP(),	@var_user_id,	UNIX_TIMESTAMP(),	@var_user_id,		@var_active_record)
	,('Performers',		1,			0,					1,					0,				UNIX_TIMESTAMP(),	@var_user_id,	UNIX_TIMESTAMP(),	@var_user_id,		@var_active_record)
	,('Consignment',	1,			0,					1,					0,				UNIX_TIMESTAMP(),	@var_user_id,	UNIX_TIMESTAMP(),	@var_user_id,		@var_active_record)
	,('Rent',			1,			0,					0,					0,				UNIX_TIMESTAMP(),	@var_user_id,	UNIX_TIMESTAMP(),	@var_user_id,		@var_active_record)
	,('Bills',			1,			0,					1,					0,				UNIX_TIMESTAMP(),	@var_user_id,	UNIX_TIMESTAMP(),	@var_user_id,		@var_active_record);

CREATE TABLE transaction (
	id				INT			NOT NULL	AUTO_INCREMENT
	,description	TEXT
	,type			INT         NOT NULL
	,method			INT			NOT NULL
	,amount			DOUBLE		NOT NULL	DEFAULT '0'
	,patron_id		INT
	,created_at		INT			NOT NULL
	,created_by     INT         NOT NULL
	,record_status  TINYINT     NOT NULL    DEFAULT '1'
	,PRIMARY KEY (id)
);

INSERT INTO transaction (description, method, type, amount, patron_id, created_at, created_by, record_status)
SELECT
	description
	,CASE
		WHEN payment_method = 'cash' THEN (SELECT id FROM transaction_method WHERE description = 'Cash')
		WHEN payment_method = 'check' THEN (SELECT id FROM transaction_method WHERE description = 'Check')
		WHEN payment_method = 'credit_card' THEN (SELECT id FROM transaction_method WHERE description = 'Credit Card')
		WHEN payment_method = 'paypal' THEN (SELECT id FROM transaction_method WHERE description = 'Paypal')
		WHEN payment_method = 'venmo' THEN (SELECT id FROM transaction_method WHERE description = 'Venmo')
		ELSE (SELECT id FROM transaction_method WHERE description = 'Other')
	END
	,CASE
		WHEN id IN (5954, 6232, 6269, 6307, 6353, 6369, 6380, 6393, 6424, 6441, 8174, 8219, 8251, 8306, 8358, 8380, 8472) THEN (SELECT id FROM transaction_type WHERE description = 'Performers')
		WHEN id IN (8173, 7994, 7529, 7500, 7497) THEN (SELECT id FROM transaction_type WHERE description = 'Consignment')
		WHEN id IN (5957) THEN (SELECT id FROM transaction_type WHERE description = 'Refund')
		WHEN type = 'membership' THEN (SELECT id FROM transaction_type WHERE description = 'Membership')
		WHEN type = 'event' THEN (SELECT id FROM transaction_type WHERE description = 'Event')
		WHEN type = 'store' THEN (SELECT id FROM transaction_type WHERE description = 'Store')
		WHEN type = 'class' THEN (SELECT id FROM transaction_type WHERE description = 'Class')
		WHEN type = 'credit' THEN (SELECT id FROM transaction_type WHERE description = 'Credit')
		WHEN type = 'donation' THEN (SELECT id FROM transaction_type WHERE description = 'Donation')
		WHEN type = 'csa' THEN (SELECT id FROM transaction_type WHERE description = 'CSA')
		ELSE (SELECT id FROM transaction_type WHERE description = 'Donation')
	END
	,CASE
		WHEN amount LIKE '-%' THEN -amount
		ELSE amount
	END
	,patron_id
	,created_at
	,user_id
	,@var_active_record
FROM transactions;

/* DROP TABLE transactions; */
